# Kotlin-Movie-Mvvm

A sample movie app for demonstration of MVVM architecture in kotlin.

## Libraries Used
--------------
  * `LiveData` - Build data objects that notify views when the underlying database changes.
  * `Navigation` - Handle everything needed for in-app navigation.
  * `Room` - Access your app's SQLite database with in-app objects and compile-time checks.
  * `ViewModel` - Store UI-related data that isn't destroyed on app rotations. Easily schedule
     asynchronous tasks for optimal execution.
  
* Third party and miscellaneous libraries
  * `Glide` for image loading
  * `Kotlin Coroutines` for managing background threads with simplified code and reducing needs for callbacks
  * `Retrofit` for api call 

## API : [yts api](https://yts.mx/api)

