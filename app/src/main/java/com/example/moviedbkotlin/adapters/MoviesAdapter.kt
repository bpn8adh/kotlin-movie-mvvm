package com.example.moviedbkotlin.adapters

import com.example.moviedbkotlin.R

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviedbkotlin.models.Movy
import kotlinx.android.synthetic.main.item_movies_preview.view.*

class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder>() {

    inner class MoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    // replaces adapter.notifydatasetchange which will update all the items
    // diff utils only updates required items, happens in background
    private val differCallback = object : DiffUtil.ItemCallback<Movy>() {
        override fun areItemsTheSame(oldItem: Movy, newItem: Movy): Boolean {
            return oldItem.url == newItem.url //for checking unique id
        }

        override fun areContentsTheSame(oldItem: Movy, newItem: Movy): Boolean {
            return oldItem == newItem
        }
    }

    // compares and updates the items that were changed
    val differ = AsyncListDiffer(this, differCallback)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        return MoviesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_movies_preview,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        val movie = differ.currentList[position]
        holder.itemView.apply {
            Glide.with(this).load(movie.background_image).into(ivArticleImage)
            tvSource.text = movie.imdb_code
            tvTitle.text = movie.title
            tvDescription.text = movie.summary
            tvPublishedAt.text = movie.date_uploaded

            setOnClickListener {
                onItemClickListener?.let {
                    it(movie)
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    private var onItemClickListener: ((Movy) -> Unit)? = null

    fun setOnItemClickListener(listener: (Movy) -> Unit) {
        onItemClickListener = listener

    }
}