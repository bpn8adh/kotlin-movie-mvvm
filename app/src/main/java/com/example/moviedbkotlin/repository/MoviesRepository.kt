package com.example.moviedbkotlin.repository

import com.example.moviedbkotlin.api.RetrofitInstance
import com.example.moviedbkotlin.db.MoviesDatabase
import com.example.moviedbkotlin.models.MoviesDetails

class MoviesRepository(val db: MoviesDatabase) {

    suspend fun getMoviesList() =
        RetrofitInstance.api.getMovies()

    suspend fun upsert(moviesDetails: MoviesDetails) = db.getMoviesDao().upsert(moviesDetails)

    fun getSavedMovies() = db.getMoviesDao().getSavedMoviesList()

}