package com.example.moviedbkotlin.utils

// recommended by google to wrap around network request
// sealed class is similar to abstract class but we can define which class can inherit from it
sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null
) {

    class Success<T>(data: T) : Resource<T>(data)
    class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)
    class Loading<T> : Resource<T>()
}