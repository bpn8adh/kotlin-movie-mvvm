package com.example.moviedbkotlin.ui.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviedbkotlin.R
import com.example.moviedbkotlin.adapters.MoviesAdapter
import com.example.moviedbkotlin.ui.viewmodels.MoviesViewModel
import com.example.moviedbkotlin.ui.MainActivity
import com.example.moviedbkotlin.utils.Resource
import kotlinx.android.synthetic.main.fragment_movies.*

class MoviesFragment : Fragment(R.layout.fragment_movies) {

    lateinit var viewModel: MoviesViewModel
    lateinit var moviesAdapter: MoviesAdapter

    val TAG = "MoviesFragment"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = (activity as MainActivity).viewModel

        setupRecyclerView()

        moviesAdapter.setOnItemClickListener {
            val bundle = Bundle().apply {
                putSerializable("movies", it)
            }
            findNavController().navigate(
                R.id.action_breakingNewsFragment_to_moviesDetailFragment,
                bundle
            )
        }

        viewModel.breakingMovies.observe(viewLifecycleOwner, Observer { response ->

            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data.let { newsResponse ->
                        moviesAdapter.differ.submitList(newsResponse?.data?.movies)
                    }
                }

                is Resource.Error -> {
                    hideProgressBar()
                    response.message.let { message ->
                        Log.e(TAG, "An error occured: $message")
                    }
                }

                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.getSavedMovies().observe(viewLifecycleOwner, Observer { moviesList ->
            Log.d("TAG", "aaaaaaaa " + moviesList)
//            moviesAdapter.differ.submitList(moviesList)
        })

    }

    private fun hideProgressBar() {
        paginationProgressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        paginationProgressBar.visibility = View.VISIBLE
    }

    private fun setupRecyclerView() {
        moviesAdapter = MoviesAdapter()
        rvBreakingNews.apply {
            adapter = moviesAdapter
            layoutManager = LinearLayoutManager(activity)
        }
    }

}