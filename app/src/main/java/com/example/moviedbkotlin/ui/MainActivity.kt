package com.example.moviedbkotlin.ui


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.moviedbkotlin.R
import com.example.moviedbkotlin.db.MoviesDatabase
import com.example.moviedbkotlin.repository.MoviesRepository
import com.example.moviedbkotlin.ui.viewmodels.MoviesViewModel
import com.example.moviedbkotlin.ui.viewmodels.MoviesViewModelProviderFactory

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MoviesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val moviesRepository = MoviesRepository(MoviesDatabase(this))
        val viewModelProviderFactory = MoviesViewModelProviderFactory(moviesRepository)

        // used by fragments
        viewModel = ViewModelProvider(this, viewModelProviderFactory)
            .get(MoviesViewModel::class.java)

    }
}
