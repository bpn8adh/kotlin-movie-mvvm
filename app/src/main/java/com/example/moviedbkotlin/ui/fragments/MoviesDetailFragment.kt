package com.example.moviedbkotlin.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.moviedbkotlin.R
import com.example.moviedbkotlin.models.Movy
import com.example.moviedbkotlin.ui.MainActivity
import com.example.moviedbkotlin.ui.viewmodels.MoviesViewModel

import kotlinx.android.synthetic.main.fragment_movies_detail.*

class MoviesDetailFragment : Fragment(R.layout.fragment_movies_detail) {
    lateinit var viewModel: MoviesViewModel

    val args: MoviesDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as MainActivity).viewModel

        val movies = args.movies

        setDataToView(movies)

    }

    private fun setDataToView(movies: Movy) {
        Glide.with(this).load(movies.background_image).into(image)

        source.text = movies.year.toString()
        title.text = movies.title
        description.text = movies.description_full
        publishedAt.text = movies.date_uploaded

    }

}
