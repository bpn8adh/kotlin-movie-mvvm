package com.example.moviedbkotlin.ui.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviedbkotlin.models.MoviesDetails
import com.example.moviedbkotlin.repository.MoviesRepository
import com.example.moviedbkotlin.utils.Resource
import kotlinx.coroutines.launch
import retrofit2.Response

class MoviesViewModel(
    val moviesRepository: MoviesRepository
) : ViewModel() {

    // fragments will get notified on changes of data due to mutable livedata
    val breakingMovies: MutableLiveData<Resource<MoviesDetails>> = MutableLiveData()


    init {
        getBreakingMovies()
    }

    /// viewModelScope.launch will ensure that coroutine will stay alive as long as viewmodel is alive
    fun getBreakingMovies() = viewModelScope.launch {
        breakingMovies.postValue(Resource.Loading())
        val response = moviesRepository.getMoviesList()
        breakingMovies.postValue(handleBreakingNewsResponse(response))
    }

    private fun handleBreakingNewsResponse(response: Response<MoviesDetails>): Resource<MoviesDetails> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                saveMovies(response.body()!!)
                Log.d("TAG", "handleBreakingNewsResponse: " + getSavedMovies())
                return Resource.Success(resultResponse)

            }
        }
        return Resource.Error(response.message())
    }

    fun saveMovies(moviesDetails: MoviesDetails) = viewModelScope.launch {
        moviesRepository.upsert(moviesDetails)
    }

    fun getSavedMovies() = moviesRepository.getSavedMovies()

}