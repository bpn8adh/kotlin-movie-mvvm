package com.example.moviedbkotlin.api

import com.example.moviedbkotlin.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitInstance {
    // companion object is similar to static in java
    companion object {

        // lazy = only initialize once
        // val = immutable. var = mutable
        private val retrofit by lazy {
            // useful for debugging and viewing logcat of api requests and responses
            val logging = HttpLoggingInterceptor()
            // to see the body of response
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)

            val client = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build()

            Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()) // converts json data to kotlin model objects
                .client(client)
                .build()
        }

        val api by lazy {
            retrofit.create(MoviesApi::class.java)
        }
    }
}