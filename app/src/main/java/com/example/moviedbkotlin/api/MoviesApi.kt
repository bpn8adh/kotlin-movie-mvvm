package com.example.moviedbkotlin.api

import com.example.moviedbkotlin.models.MoviesDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {

    @GET("v2/list_movies.json")
    suspend fun getMovies(
    ): Response<MoviesDetails>

}