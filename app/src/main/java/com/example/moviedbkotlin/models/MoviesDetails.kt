package com.example.moviedbkotlin.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


@Entity(tableName = "moviedetail")
data class MoviesDetails(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("@meta")
    val meta: Meta?,
    val `data`: Data,
    val status: String,
    val status_message: String
)