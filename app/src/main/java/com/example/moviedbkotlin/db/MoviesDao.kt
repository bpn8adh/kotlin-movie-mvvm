package com.example.moviedbkotlin.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.moviedbkotlin.models.MoviesDetails
import com.example.moviedbkotlin.models.Movy

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(moviesDetails: MoviesDetails): Long // update or insert

    @Query("SELECT * FROM moviedetail")
    fun getSavedMoviesList(): LiveData<List<MoviesDetails>>

}