package com.example.moviedbkotlin.db

import android.util.Log
import androidx.room.TypeConverter
import com.example.moviedbkotlin.models.Data
import com.example.moviedbkotlin.models.Meta
import com.example.moviedbkotlin.models.Movy
import com.example.moviedbkotlin.models.Torrent
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {
    @TypeConverter
    fun fromMovies(movies: List<Movy>): String {
        val type = object : TypeToken<List<Movy>>() {}.type
        return Gson().toJson(movies, type)
    }

    @TypeConverter
    fun toMovies(moviesString: String): List<Movy> {
        val type = object : TypeToken<List<Movy>>() {}.type
        return Gson().fromJson<List<Movy>>(moviesString, type)
    }

    @TypeConverter
    fun fromMeta(meta: Meta?): String? {
        val type = object : TypeToken<Meta?>() {}.type
        return Gson().toJson(meta, type)
    }

    @TypeConverter
    fun toMeta(metaString: String?): Meta? {
        val type = object : TypeToken<Meta>() {}.type
        return Gson().fromJson(metaString, type)
    }

    @TypeConverter
    fun fromData(`data`: Data): String {
        val type = object : TypeToken<Data>() {}.type
        return Gson().toJson(data, type)
    }

    @TypeConverter
    fun toData(dataString: String): Data {
        val type = object : TypeToken<Data>() {}.type
        return Gson().fromJson<Data>(dataString, type)
    }

    @TypeConverter
    fun fromGenres(genres: List<String>): String {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().toJson(genres, type)
    }

    @TypeConverter
    fun toGenres(genreString: String): List<String> {
        val type = object : TypeToken<List<String>>() {}.type
        return Gson().fromJson<List<String>>(genreString, type)
    }

    @TypeConverter
    fun fromTorrent(torrents: List<Torrent>): String {
        val type = object : TypeToken<List<Torrent>>() {}.type
        return Gson().toJson(torrents, type)
    }

    @TypeConverter
    fun toTorrent(torrentsString: String): List<Torrent> {
        val type = object : TypeToken<List<Torrent>>() {}.type
        return Gson().fromJson<List<Torrent>>(torrentsString, type)
    }


}