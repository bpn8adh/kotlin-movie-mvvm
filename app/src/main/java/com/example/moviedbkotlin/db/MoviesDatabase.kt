package com.example.moviedbkotlin.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.moviedbkotlin.models.MoviesDetails

@Database(
    entities = [MoviesDetails::class],
    version = 1
)

@TypeConverters(Converters::class)
abstract class MoviesDatabase : RoomDatabase() {

    abstract fun getMoviesDao(): MoviesDao

    // for singleton class instance
    companion object {
        // volatile = other threads can see if it changes instance
        @Volatile
        private var instance: MoviesDatabase? = null
        private val LOCK = Any()

        // called when instance of ArticleDatabase() is called
        // synchronized = cannot be accessed by other thread at same time
        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                MoviesDatabase::class.java,
                "movie_db.db"
            ).build()

    }
}